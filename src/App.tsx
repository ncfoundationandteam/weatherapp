import React from 'react';
import './App.css';
import { Container } from "@material-ui/core";
import Header from './component/header/header';
import Footer from './component/footer/footer';
import Search from './component/search/search';
import WeatherDetails from './component/weather-details/weather-details';


function App() {

  return (
    <div  className="text-center">
      <Header />
      <Container className="min-h-main-container" maxWidth="sm">
        <div className="flex items-center h-40">
          <Search />
        </div>
        <div className="">
          <WeatherDetails />
        </div>
      </Container>
      <Footer />
    </div>
  );
}

export default App;
