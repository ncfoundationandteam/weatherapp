import React from "react";
import { Grid, Paper, Typography } from "@material-ui/core";


const data = {
      "getCityByName": {
        "id": "1275004",
        "name": "Kolkata",
        "country": "IN",
        "coord": {
          "lon": 88.3697,
          "lat": 22.5697
        },
        "weather": {
          "summary": {
            "title": "Haze",
            "description": "haze",
            "icon": "50n"
          },
          "temperature": {
            "actual": 304.12,
            "feelsLike": 311.12,
            "min": 304.12,
            "max": 304.12
          },
          "wind": {
            "speed": 3.6,
            "deg": 270
          },
          "clouds": {
            "all": 75,
            "visibility": 2800,
            "humidity": 84
          },
          "timestamp": 1627393949
        }
      }
  };

export default function WeatherDetails() {

  return (
    <div className="flex-grow">
      <Grid item xs={12}>
        <Paper className="p-4 text-center flex flex-col color-secondary">
          <div className="flex flex-grow">
            <Typography variant="h4" className="flex-grow text-left">
              {data.getCityByName.name}
            </Typography>
            <Typography variant="h4" className="">
            {data.getCityByName.weather.temperature.actual}
            </Typography>
          </div>
          <div className="flex">
              {data.getCityByName.weather.summary.description}
          </div>
        </Paper>
      </Grid>
    </div>
  );
}
