import React from "react";

export default function Footer() {

  return <footer className="flex-grow font-light text-xs">© 2021 NCandTeam</footer>;
}
