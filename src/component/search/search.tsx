import React from "react";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";

export default function Search() {

  return (
    <div className="flex-grow m-r-4 m-l-0 w-full rounded relative sm:w-auto border border-gray-300 hover:border-gray-500">
      <div className="px-4 py-0 h-full absolute flex items-center	justify-center pointer-events-none opacity-40">
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Search City…"
        classes={{
          root: "w-full",
          input: "w-full p-2 pl-10 ",
        }}
        inputProps={{ "aria-label": "search" }}
      />
    </div>
  );
}
