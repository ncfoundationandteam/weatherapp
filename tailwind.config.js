module.exports = {
  important: true,
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    minHeight: {
      '0': '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      'full': '100%',
      'main-container': 'calc(100vh - 6rem)'
    },
    colors: {
      primary: "var(--theme-primary)",
      secondary: "var(--theme-secondary)",
    },
    extend: {
      colors: {
        gray: {
          darkest: '#1f2d3d',
          dark: '#3c4858',
          DEFAULT: '#c0ccda',
          light: '#e0e6ed',
          lightest: '#f9fafc',
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
